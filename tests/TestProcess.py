import unittest
import os
import csv
import logging
import sqlite3
from datetime import datetime
from Process import *

class TestStockageSIV(unittest.TestCase):

    def setUp(self):
        init_logging()
        #Ecriture d'un fichier csv test
        csvfile = open('testauto.csv', 'w')
        csvfile.write('')
        csvfile.close()
        with open('testauto.csv', 'a', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=';')
            writer.writerow(('adresse_titulaire','nom','prenom','immatriculation','date_immatriculation','vin','marque','denomination_commerciale','couleur','carrosserie','categorie','cylindre','energie','places','poids','puissances','type','variante','version'))
            writer.writerow(('3822 Omar Square Suite 257 Port Emily, OK 43251','Smith','Jerome','OVC-568','03/05/2012','9780082351764','Williams Inc','Enhanced well-modulated moderator','LightGoldenRodYellow','45-1743376','34-7904216','3462','37578077','32','3827','110','Inc','92-3625175','79266482'))
            writer.writerow(('974 Byrd Mountains New Jennifer, IL 01612','Olson','Paul','859 GZP','14/06/2019','9781246585674','Ross PLC','Right-sized secondary array','Snow','03-0258606','71-7143342','4837','15895400','9','5870','298','LLC','37-7112501','39890658'))
            writer.writerow(('604 Willis View Suite 279 Hansenview, NY 26033','Gomez','Jose','2-2270C','29/11/1988','9781027382348','Gillespie PLC','Balanced intangible portal','CadetBlue','26-7045508','16-3556230','1245','99868567','33','3504','406','PLC','67-9571998','92859586'))
            writer.writerow(('3822 Omar Square Suite 257 Port Emily, OK 43251','Jhon','Jerome','OVC-568','03/05/2012','9780082351764','Williams Inc','Enhanced well-modulated moderator','LightGoldenRodYellow','45-1743376','34-7904216','3462','37578077','32','3827','110','Inc','92-3625175','79266482'))
            writer.writerow(('3822 Omar Square Suite 257 Port Emily, OK 43251','Smith','Jer--ome','OVC-568','03"/05/2012','9780082351764','Williams Inc','Enhanced well-modulated moderator','LightGoldenRodYellow','45-1743376','34-7904216','3462','37578077','32','3827','110','Inc','92-3625175','79266482'))
           
        self.liste_ligne = []
        with open('testauto.csv') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            for row in reader:
                self.liste_ligne.append(row)
        #Ecriture d'un fichier sqLite3 test et d'un self.cursor
        self.con = sqlite3.connect('testcars.sqlite3')
        self.cursor = self.con.cursor()

    def test_check_table(self):
        self.assertEqual(check_table(self.cursor), False)
        self.cursor.execute('''CREATE TABLE titine (
        adresse_titulaire        TEXT,
        nom                      TEXT,
        prenom                   TEXT,
        immatriculation          TEXT,
        date_immatriculation     TEXT,
        vin                      TEXT,
        marque                   TEXT,
        denomination_commerciale TEXT,
        couleur                  TEXT,
        carrosserie              TEXT,
        categorie                TEXT,
        cylindre                 TEXT,
        energie                  TEXT,
        places                   TEXT,
        poids                    TEXT,
        puissances               TEXT,
        type                     TEXT,
        variante                 TEXT,
        version                  TEXT
        )'''
        )
        self.con.commit()
        self.assertEqual(check_table(self.cursor), True)
    
    def test_Creation_Table(self):
        self.assertEqual(check_table(self.cursor), False)
        creationTable(self.cursor)
        self.con.commit()
        self.assertEqual(check_table(self.cursor), True)

    def test_execution(self):
        creationTable(self.cursor)
        self.cursor.execute('''INSERT INTO siv (adresse_titulaire, nom, prenom, immatriculation, date_immatriculation, vin, marque, denomination_commerciale, couleur, carrosserie, categorie, cylindre, energie, places, poids, puissances, type ,variante, version)
            VALUES ('3822 Omar Square Suite 257 Port Emily, OK 43251','Smith','Jerome','OVC-568','03/05/2012',9780082351764,'Williams Inc','Enhanced well-modulated moderator','LightGoldenRodYellow','45-1743376','34-7904216',3462,37578077,32,3827,110,'Inc','92-3625175',79266482)
            ''')
        self.cursor.execute('''INSERT INTO siv (adresse_titulaire, nom, prenom, immatriculation, date_immatriculation, vin, marque, denomination_commerciale, couleur, carrosserie, categorie, cylindre, energie, places, poids, puissances, type ,variante, version)
            VALUES ('974 Byrd Mountains New Jennifer, IL 01612','Olson','Paul','859 GZP','14/06/2019',9781246585674,'Ross PLC','Right-sized secondary array','Snow','03-0258606','71-7143342',4837,15895400,9,5870,298,'LLC','37-7112501',39890658)
            ''')
        self.cursor.execute('''INSERT INTO siv (adresse_titulaire, nom, prenom, immatriculation, date_immatriculation, vin, marque, denomination_commerciale, couleur, carrosserie, categorie, cylindre, energie, places, poids, puissances, type ,variante, version)
            VALUES ('604 Willis View Suite 279 Hansenview, NY 26033','Gomez','Jose','2-2270C','29/11/1988',9781027382348,'Gillespie PLC','Balanced intangible portal','CadetBlue','26-7045508','16-3556230',1245,99868567,33,3504,406,'PLC','67-9571998',92859586)
            ''')
        self.con.commit()
        self.assertEqual(check_execution(('OVC-568',), self.cursor), True)
        self.assertEqual(check_execution(('859 GZP',), self.cursor), True)
        self.assertEqual(check_execution(('2-2270C',), self.cursor), True)
        self.assertEqual(check_execution(('PVC-567',), self.cursor), False)
        self.assertEqual(check_execution(('OOPC-8',), self.cursor), False)

    def test_execution(self):
        creationTable(self.cursor)
        execution(self.list_ligne[0], self.cursor)
        execution(self.list_ligne[1], self.cursor)
        execution(self.list_ligne[2], self.cursor)
        self.con.commit()
        for row in self.cursor.execute('''SELECT nom FROM siv WHERE immatriculation = 'OVC-568' '''):
            data = row[0]
        self.assertEqual(data, 'Smith')
        for row in self.cursor.execute('''SELECT prenom FROM siv WHERE marque = 'Ross PLC' '''):
            data = row[0]
        self.assertEqual(data, 'Paul')
        for row in self.cursor.execute('''SELECT adresse_titulaire FROM siv WHERE prenom = 'Jose' '''):
            data = row[0]
        self.assertEqual(data, '604 Willis View Suite 279 Hansenview, NY 26033')

    def test_netoyage(self):
            creationTable(self.cursor)
            netoyage(self.list_ligne[0], self.cursor)
            self.con.commit()
            for row in self.cursor.execute('''SELECT nom FROM siv WHERE immatriculation = 'OVC-568' '''):
                data = row[0]
            self.assertEqual(data, 'Smith')

            netoyage(self.list_ligne[3], self.cursor)
            self.con.commit()
            for row in self.cursor.execute('''SELECT nom FROM siv WHERE immatriculation = 'OVC-568' '''):
                data = row[0]
            self.assertEqual(data, 'Jhon')
            
            netoyage(self.list_ligne[4], self.cursor)
            self.con.commit()
            for row in self.cursor.execute('''SELECT prenom FROM siv WHERE immatriculation = 'OVC-568' '''):
                data = row[0]
            self.assertEqual(data, 'Jerome')
            for row in self.cursor.execute('''SELECT date_immatriculation FROM siv WHERE immatriculation = 'OVC-568' '''):
                data = row[0]
            self.assertEqual(data, '03/05/2012')

    def test_netoyage_csv(self):
            creationTable(self.cursor)
            netoyage_csv('testauto.csv', self.cursor)
            self.con.commit()
            for row in self.cursor.execute('''SELECT nom FROM siv WHERE immatriculation = 'OVC-568' '''):
                data = row[0]
            self.assertEqual(data, 'Smith')
            for row in self.cursor.execute('''SELECT prenom FROM siv WHERE marque = 'Ross PLC' '''):
                data = row[0]
            self.assertEqual(data, 'Paul')
            for row in self.cursor.execute('''SELECT adresse_titulaire FROM siv WHERE prenom = 'Jose' '''):
                data = row[0]
            self.assertEqual(data, '604 Willis View Suite 279 Hansenview, NY 26033')

    def test_init_logging(self):        
        file_name = './Logs/{:%Y-%m-%d-%H-%M-%S}.log'.format(datetime.now())
        self.assertEqual(os.path.exists(file_name), True)

    def test_check_CSV(self):
        #Ecriture d'un fichier csv vide et d'un fichier text test
        self.assertEqual(check_csv('testauto.csv'), True)

    def tearDown(self):
        self.con.close()
        os.remove('testauto.csv')
        os.remove('testcars.sqlite3')
