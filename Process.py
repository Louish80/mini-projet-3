#Imports
import sqlite3
import logging
import csv
from copy import copy
import unicodedata
import sys
import os.path
import os
from datetime import datetime

logging_path=('./log/') #emplacement des logs
bdd_path=('./cars.sqlite3') #emplacement de la DB
csv_path=('./auto.csv') #emplacement du csv

def init_logging():
    '''
    initiation du logging répertoriant les actions effectuées (asctime) avec leur niveau (levelname) et le descriptif (message)
    '''
    logging.basicConfig(format ='%(asctime)s : %(levelname)s : Process.py : %(message)s',filename=check_logging(),level=logging.DEBUG) 
    #configuration du module logging

def check_logging():
    '''
    %OUT: file_name (nom du fichier logging)
    On vérifie le chemin au logging existe et si 
    OUI on vérifie si c'est un dossier ou un fichier 
    SINON on créé le dossier file_name sera le dossier existant  
    '''
    if os.path.exists(logging_path): #Test si l'élément log_path existe
        if os.path.isdir(logging_path): #test si c'est un dossier
            file_name = (logging_path + datetime.now().strftime("%Y-%m-%d") +'.log')
        else: #sinon on arrete tout
            print("Erreur log est un fichier")
            exit(1)
    else: #sinon on le cree
        try:
            os.mkdir(logging_path)
            file_name = (logging_path + datetime.now().strftime("%Y-%m-%d") +'.log')
        except OSError as error:
                print(error.__cause__)
                exit(1)
    return file_name

def connection_bdd():
    '''
    On vérifie si le fichier de la base de données existe (cas.sqlite3) si
    OUI on essaie de s'y connecter, en cas de problème d'accès un warning est envoyé au logging 
    et un message d'erreur s'affiche
    '''
    if os.path.exists(bdd_path): #test si le fichier de DB existe
        try:
            logging.info("Connexion à la base de donnée")
            return sqlite3.connect(bdd_path)
        except sqlite3.Error as identifier:
            logging.warning("Erreur lors dee l'accès à la base de donnée")
            print("Erreur lors dee l'accès à la base de donnée")

def check_table(cursor):
    '''
    %IN: cursor (moyen de connexion à la BD)
    %OUT: Booléen (True si la requête a aboutie, False sinon)
    On vérifie l'éxistence de la table titine en envoyant une information au logging
    puis en essayant d'executer une requête selectionnant les immatriculation
    si elle n'aboutie pas on informe le logging que la table n'existe pas
    '''
    logging.info("Vérifiacation de l'existence de la table titine")
    try:
        cursor.execute("SELECT immatriculation FROM titine")
    except:
        logging.info("La table n'existe pas")
        return False
    return True

def creationTable(cursor):
    '''
    %IN: cursor (moyen de connexion à la BD)
    On informe le logging d'une création de table et 
    on effectue une requête de création de table avec les attributs souhaités,
    on informe le logging que la table est crée ou on lui envoie une erreur lors de la création
    '''
    try:
        logging.debug("Creation de la table")
        cursor.execute('''CREATE TABLE titine (
        adresse_titulaire        TEXT,
        nom                      TEXT,
        prenom                   TEXT,
        immatriculation          TEXT,
        date_immatriculation     TEXT,
        vin                      TEXT,
        marque                   TEXT,
        denomination_commerciale TEXT,
        couleur                  TEXT,
        carrosserie              TEXT,
        categorie                TEXT,
        cylindre                 TEXT,
        energie                  TEXT,
        places                   TEXT,
        poids                    TEXT,
        puissances               TEXT,
        type                     TEXT,
        variante                 TEXT,
        version                  TEXT
        );
        ''')
        logging.info("table crée")
    except sqlite3.Error as identifier:
        logging.warning("creation de la table impossible")
        print("Erreur lors de la création de la table")

def check_csv(csv_path):
    '''
    %IN:csv_path (chemin du fichier csv)
    %OUT: booléen (True si le CSV existe ET est un fichier ET contient des éléments sinon False)
    On vérifie si le fichier CSV existe (sinon envoie d'un danger au logging et un message d'erreur)
               si il est bien un fichier (sinon envoie d'un danger au logging et un message d'erreur)
               si il n'est pas vide (sinon envoie d'un danger au logging et un message d'erreur)

    '''
    if not os.path.exists(csv_path):
        logging.warning("Le fichier n'existe pas")
        print("Le fichier n'existe pas !")
        return False
    if not os.path.isfile(csv_path):
        logging.warning("Le fichier n'est pas un fichier")
        print("Le fichier n'est pas un fichier !")
        return False
    if os.path.getsize(csv_path)<=10:
        logging.warning("Le fichier est vide")
        print("Le fichier est vide !")
        return False
    return True

def netoyage_csv(csv_path, cursor):
    '''
    %IN: csv_path (chemin du fichier csv), cursor (moyen de connexion à la BD)
    Envoie d'une information de lecture de csv au logging, on lit le csv 
    et on le nettoie en appelant la fonction nettoyage  
    '''
    logging.info("Lecture du fichier csv")
    with open(csv_path) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        for row in reader:
            netoyage(row, cursor)

def netoyage(row, cursor):
    '''
    %IN: row (lignes du CSV), cursor (moyen de connexion à la BD)
    Envoie l'information de nettoyage au logging et 
    on nettoie chaque champs de la ligne en supprimants des caractères superflues
    '''
    logging.info("Netoyage des lignes")
    for key, value in row.items():
        row[key] = row[key].replace("--", "").replace('"', "")
    execution(row, cursor)

def execution(row, cursor):
    '''
    %IN: row (lignes du CSV), cursor (moyen de connexion à la BD)
    On test si des informations ne sont pas déjà rentrées avec le fonction check_execution
    si OUI on informe le logging qu'on met à jour la table titine et on execute une commande de mise à jour
    SINON on informe le logging qu'on insert des données et on execute une commande d'insertion
    '''
    if check_execution(row, cursor):
        logging.info("Mise à jour de la table titine")

        cursor.execute('''UPDATE titine SET adresse_titulaire = :adresse_titulaire, nom = :nom, prenom = :prenom, 
        immatriculation = :immatriculation, date_immatriculation = :date_immatriculation, vin = :vin, marque = :marque,
        denomination_commerciale = :denomination_commerciale, couleur = :couleur, carrosserie = :carrosserie,
        categorie = :categorie, cylindre = :cylindre, energie = :energie, places = :places, poids = :poids, 
        puissances = :puissances, type = :type, variante = :variante, version = :version WHERE immatriculation = :immatriculation''', row)
    else:
        logging.info("Insertion dans la table titine")

        cursor.execute('''INSERT INTO titine VALUES (:adresse_titulaire, :nom, :prenom, :immatriculation, 
                :date_immatriculation, :vin, :marque, :denomination_commerciale, :couleur, :carrosserie, :categorie, 
                :cylindre, :energie, :places, :poids, :puissances, :type, :variante, :version)''', row)

def check_execution(row, cursor):
    '''
    %IN: row (lignes du CSV), cursor (moyen de connexion à la BD)
    %OUT: booléen (True si on reçoie des information de la BD, False si on ne reçoit rien)
    On effectue une requête pour savoir si il y a déjà des informations dans la table titine
    '''
    cursor.execute('SELECT immatriculation FROM titine WHERE immatriculation= :immatriculation', row)
    if not cursor.fetchall():
        return False
    return True

if __name__ == '__main__' :
    '''
    Programme principal faisant appel aux fonctions précédentes
    Si tout se déroule bien nous affiche un message de fin de programme 
    ainsi que cette même information au logging
    '''
    log = init_logging()
    bdd = connection_bdd()
    cursor = bdd.cursor()
    if not check_table(cursor):
        creationTable(cursor)
    bdd.commit()
    if check_csv(csv_path):
        netoyage_csv(csv_path, cursor)
    else:
        print("Fin du programme")
    bdd.commit()
    logging.info("Fin du programme")    